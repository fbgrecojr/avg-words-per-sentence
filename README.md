# Average Words Per Sentence

## Overview

This **C** program computes the average number of words per senctence.

## Quickstart

Provided is a test file that contains the text for the Preamble of the *U.S. Declaration of Independence*.

1. Navigate to the project directory
2. Compile the project: `make`
2. Run the project: `./main test.txt`

**Output:** `Your body of text contains 201 words and 4 sentences resulting in an average of 50.25 words per sentence`

## License
MIT License
